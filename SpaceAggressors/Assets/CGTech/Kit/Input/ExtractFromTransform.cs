﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Extract From Transform")]
    public class ExtractFromTransform : VectorSource
    {

        public enum PropertyName
        {
            Undefined = 0,
            Position,
            RotationEuler
        }
        [Header("Input")]
        [SerializeField]
        private Transform m_target;
        [SerializeField]
        private PropertyName m_propertyName = PropertyName.Position;
        [Header("Output")]
        [SerializeField]
        private Vector2 m_currentValue;

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_target != null)
            {
                GizmoHelper.DrawArrow(m_target.position, transform.position, GizmoHelper.KitType.Vector);
            }
        }

        public override Vector2 Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_target != null)
                {
                    switch (m_propertyName)
                    {
                        case PropertyName.Position:
                            m_currentValue = m_target.position;
                            break;
                        case PropertyName.RotationEuler:
                            m_currentValue = m_target.rotation.eulerAngles;
                            break;
                    }
                }

            }
            return m_currentValue;
        }

        
    }
}