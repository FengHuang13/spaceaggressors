﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Find Object By Name")]
    public class FindObjectByName : GameObjectSource
    {

        [Header("Input")]
        [SerializeField]
        private string m_name;
        [Header("Output")]
        [SerializeField]
        private GameObject m_currentValue;
        

        
        public override GameObject Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_currentValue == null || m_currentValue.name != m_name)
                {
                    m_currentValue = GameObject.Find(m_name);
                }
            }
            return m_currentValue;
        }
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_currentValue != null)
            {
                GizmoHelper.DrawArrow(m_currentValue.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
        }

    }
}