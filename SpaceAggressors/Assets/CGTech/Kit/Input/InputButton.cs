﻿using System;
using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Input
{
    public class InputButton : BooleanSource
    {
        

        [Header("Inputs")]
        [SerializeField]
        private BooleanBehaviour m_behaviour = BooleanBehaviour.OnTrue;
        public string m_axisName;
        [Header("Output")]
        [SerializeField]
        private bool m_currentValue = false;



        public override bool Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                switch (m_behaviour)
                {
                    case BooleanBehaviour.Continuous:
                        m_currentValue = UnityEngine.Input.GetButton(m_axisName);
                        break;
                    case BooleanBehaviour.OnTrue:
                        m_currentValue = UnityEngine.Input.GetButtonDown(m_axisName);
                        break;
                    case BooleanBehaviour.OnFalse:
                        m_currentValue = UnityEngine.Input.GetButtonUp(m_axisName);
                        break;
                    case BooleanBehaviour.Once:
                        m_currentValue = m_currentValue | UnityEngine.Input.GetButton(m_axisName);
                        break;
                    default:
                        m_currentValue = false;
                        break;
                }
            }
            return m_currentValue;
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Input;
            }
        }

        protected override void DrawGizmos()
        {
            
        }
        #endregion

    }
}
