﻿using System;
using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Input Axis Value")]
    public class InputAxisValue : FloatingPointSource
    {
        [Header("Inputs")]
        [SerializeField]
        private string m_AxisName = "";
        [Header("Process Options")]
        [SerializeField]
        [Tooltip("Tick to use unsmoothed input values")]
        private bool m_useRawValue = false;
        [Header("Output")]
        [SerializeField]
        private float m_currentValue;



        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Input;
            }
        }
        protected override void DrawGizmos()
        {
        }

        public override float Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                m_currentValue = Calculate(); 
            }
            return m_currentValue;
        }

        protected float Calculate()
        {
            float axisValue;
            if (m_useRawValue)
                axisValue = UnityEngine.Input.GetAxisRaw(m_AxisName);
            else
                axisValue = UnityEngine.Input.GetAxis(m_AxisName);
            m_currentValue = axisValue;
            return axisValue;
        }
        #endregion

    }


}
