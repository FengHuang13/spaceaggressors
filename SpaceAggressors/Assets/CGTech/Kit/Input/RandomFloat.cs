﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Random Floating Point")]
    public class RandomFloat : FloatingPointSource
    {

        public enum PropertyName
        {
            Undefined = 0,
            Position,
            RotationEuler
        }
        [Header("Input")]
        [SerializeField]
        private BooleanSource m_makeNewValue = null;
        [Header("Output")]
        [SerializeField]
        private float m_currentValue;
        private float lastValue = float.NaN;


        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_makeNewValue != null)
            {
                GizmoHelper.DrawArrow(m_makeNewValue.transform.position, transform.position, GizmoHelper.KitType.Vector);
            }
        }

        public override float Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame || force)
            {
                m_calculatedThisFrame = true;
                m_currentValue = Calculate();

            }
            return m_currentValue;
        }

        protected float Calculate()
        {


            if (float.IsNaN(lastValue) ||
                (m_makeNewValue != null && m_makeNewValue.Fetch()))
            {
                lastValue = UnityEngine.Random.Range(0f, 1f);
            }


            m_currentValue = lastValue;
            return m_currentValue;
        }
    }
}