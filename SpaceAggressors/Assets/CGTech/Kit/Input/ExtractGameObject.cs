﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Extract Game Object")]
    public class ExtractGameObject : GameObjectSource
    {

        [Header("Input")]
        [SerializeField]
        private GameObject m_target;
        [Header("Output")]
        [SerializeField]
        private GameObject m_currentValue;



        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_target != null)
            {
                GizmoHelper.DrawArrow(m_target.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
        }

        public override GameObject Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_target != null)
                {
                    m_currentValue = m_target;
                }

            }
            return m_currentValue;
        }

    }
}