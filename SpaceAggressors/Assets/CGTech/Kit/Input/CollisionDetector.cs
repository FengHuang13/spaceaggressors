﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Anglia.CGTech.CKit.Helper;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Collision Detector")]
    public class CollisionDetector : GameObjectSource
    {
        //[Header("Inputs")]
        //[SerializeField]
        //private BooleanBehaviour m_behaviour = BooleanBehaviour.OnTrue;

        [Header("Output")]
        [SerializeField]
        private GameObject m_currentValue;
        [SerializeField]
        private GameObject m_lastDetected;
        private bool m_detected;

        void OnCollisionEnter2D(Collision2D collision2D)
        {
            m_lastDetected = collision2D.gameObject;
            m_detected = false;
        }

        void OnTriggerEnter2D(Collider2D collider2D)
        {
            m_lastDetected = collider2D.gameObject;
            m_detected = true;
        }

        public override GameObject Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_detected)
                {
                    m_currentValue = m_lastDetected;
                }
                else
                    m_currentValue = null;
            }
            return m_currentValue;
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }
        protected override void DrawGizmos()
        {
            
        }


        #endregion
    }
}