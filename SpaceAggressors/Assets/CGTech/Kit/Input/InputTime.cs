﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Input Time")]
    public class InputTime : FloatingPointSource
    {
        [Header("Output")]
        [SerializeField]
        private float m_currentValue;

        public override float Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                m_currentValue = Time.deltaTime;
            }
            return m_currentValue;
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }
        protected override void DrawGizmos()
        {
        }

        
        #endregion
    }
}