﻿using Anglia.CGTech.CKit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anglia.CGTech.CKit.Helper;
using System;

namespace Anglia.CGTech.CKit.Input
{
    public class RayCast : GameObjectSource
    {
        [Header("Inputs")]
        [SerializeField]
        private VectorSource m_RayVector = null;
        [SerializeField]
        private Vector2 ray = Vector2.zero;
        [SerializeField]
        private string[] m_matchTags;
        [Header("Output")]
        [SerializeField]
        private GameObject m_currentValue;

        public override GameObject Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                bool foundTarget = false;
                
                if (m_RayVector != null)
                {
                    ray = m_RayVector.Fetch();
                    
                    RaycastHit2D[] hitsInfo = Physics2D.RaycastAll(transform.position, ray);
                    for (int i = 0; i < hitsInfo.Length; i++)
                    {
                        Debug.Log(hitsInfo[i].collider.gameObject.name);
                        string currentTag = hitsInfo[i].collider.gameObject.tag;
                        if (m_matchTags.Length == 0)
                        {
                            m_currentValue = hitsInfo[i].collider.gameObject;
                            foundTarget = true;
                        }
                        else
                        {
                            for (int m = 0; m < m_matchTags.Length; m++)
                            {
                                if (m_matchTags[m] == currentTag)
                                {
                                    m_currentValue = hitsInfo[i].collider.gameObject;
                                    foundTarget = true;
                                    break;
                                }
                            }
                        }
                        if (foundTarget)
                        {
                            break;
                        }
                    }
                    if (!foundTarget)
                    {
                        m_currentValue = null;
                    }
                }
            }
            return m_currentValue;
        }

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }


        protected override void DrawGizmos()
        {
            if (m_currentValue != null)
            {
                GizmoHelper.DrawArrow(m_currentValue.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
            GizmoHelper.DrawArrow(transform.position, transform.position + (Vector3)ray, GizmoHelper.KitType.Vector);
        }
    }
}