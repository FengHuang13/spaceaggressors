﻿using UnityEngine;
namespace Anglia.CGTech.CKit.Helper
{
    public static class GizmoHelper
    {
        public enum GizmoColor
        {
            Undefined = 0,
            StoreValue,
            RetreiveValue,
            ModifyValue
        }

        public enum KitType
        {
            Undefined=0,
            Integer,
            Float,
            Vector,
            Boolean,
            GameObject,
            String
        }

        public enum PartType
        {
            Undefined=0,
            Discovery,
            Input,
            Storage,
            Process,
            Effect,
            Block
        }

        public static Color TypeColor(KitType t)
        {
            Color result = Color.black;
            switch(t)
            {
                case KitType.Integer:
                    result = Color.blue;
                    break;
                //case KitType.Vector:
                //    result = Color.clear;
                //    break;
                case KitType.Vector:
                    result = Color.cyan;
                    break;
                //case KitType.GameObject:
                //    result = Color.gray;
                //    break;
                //case KitType.Integer:
                //    result = Color.green;
                //    break;
                case KitType.Boolean:
                    result = Color.magenta;
                    break;
                case KitType.String:
                    result = Color.red;
                    break;
                //case KitType.String:
                //    result = Color.white;
                //    break;
                case KitType.Float:
                    result = Color.yellow;
                    break;
                default:
                    result = Color.black;
                    break;

            }


            return result;
        }

        public static IconManager.LabelIcon TypeGizmo(PartType t)
        {
            IconManager.LabelIcon result = IconManager.LabelIcon.Gray;
            switch (t)
            {
                case PartType.Discovery:
                    result = IconManager.LabelIcon.Green;
                    break;
                case PartType.Input:
                    result = IconManager.LabelIcon.Blue;
                    break;
                case PartType.Storage:
                    result = IconManager.LabelIcon.Yellow;
                    break;
                case PartType.Process:
                    result = IconManager.LabelIcon.Orange;
                    break;
                case PartType.Effect:
                    result = IconManager.LabelIcon.Red;
                    break;
                case PartType.Block:
                    result = IconManager.LabelIcon.Gray;
                    break;
                default:
                    result = IconManager.LabelIcon.None;
                    break;

            }


            return result;
        }

        public static void DrawArrow(Vector3 from, Vector3 to, GizmoColor color)
        {
            //SetColor(color);
            //Gizmos.DrawLine(from, to);
            //Vector3 midpoint = (to-from)*.5f + from;
            //Vector3 arrowpoint = (to - from) * .55f + from;
            
            //Vector3 scaleV = (arrowpoint - midpoint);
            //Vector3 leftPoint = midpoint;
            //leftPoint.x += scaleV.y;
            //leftPoint.y -= scaleV.x;

            //Gizmos.DrawLine(leftPoint, arrowpoint);

            //Vector3 rightPoint = midpoint;
            //rightPoint.x -= scaleV.y;
            //rightPoint.y += scaleV.x;

            //Gizmos.DrawLine(rightPoint, arrowpoint);

        }

        public static void DrawArrow(Vector3 from, Vector3 to, KitType kType)
        {
            Gizmos.color=TypeColor(kType);

            Gizmos.DrawLine(from, to);
            Vector3 midpoint = (to - from) * .5f + from;
            Vector3 arrowpoint = (to - from) * .55f + from;

            Vector3 scaleV = (arrowpoint - midpoint);
            Vector3 leftPoint = midpoint;
            leftPoint.x += scaleV.y;
            leftPoint.y -= scaleV.x;

            Gizmos.DrawLine(leftPoint, arrowpoint);

            Vector3 rightPoint = midpoint;
            rightPoint.x -= scaleV.y;
            rightPoint.y += scaleV.x;

            Gizmos.DrawLine(rightPoint, arrowpoint);

        }

        private static void SetColor(GizmoColor color)
        {
            Color result = Color.white;
            switch (color)
            {
                case GizmoColor.StoreValue:
                    result = Color.red;
                    break;
                case GizmoColor.RetreiveValue:
                    result = Color.yellow;
                    break;
                case GizmoColor.ModifyValue:
                    result = Color.magenta;
                    break;
            }
            Gizmos.color = result;
        }

        public static void SetUnityGizmo(GameObject target, PartType partType)
        {
            IconManager.SetIcon(target, TypeGizmo(partType));
        }
    }
}
