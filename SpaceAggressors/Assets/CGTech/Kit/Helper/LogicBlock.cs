﻿using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LogicBlock : GizmoControl
{
    [SerializeField]
    [Range(1f, 2f)]
    private float scaleFactor = 1.1f;
    [SerializeField]
    private Color boxColor = new Color(1f,1f,1f,0.25f);
    [SerializeField]
    private bool m_FirstGenerationOnly = true;

    protected override GizmoHelper.PartType PartType
    {
        get
        {
            return GizmoHelper.PartType.Block;
        }
    }
    protected override void DrawGizmos()
    {
        Bounds boundary = new Bounds(transform.position, Vector3.zero);
        boundary = EncapsulateChildren(boundary,transform);
        Gizmos.color = boxColor;
        Vector3 offset = transform.position - boundary.center;
        Gizmos.DrawCube(boundary.center - offset * (scaleFactor - 1f), boundary.size * scaleFactor);
    }

    private Bounds EncapsulateChildren(Bounds boundary, Transform transform)
    {

        for (int i = 0; i < transform.childCount; i++)
        {
            boundary.Encapsulate(transform.GetChild(i).position);
            if (!m_FirstGenerationOnly)
            {
                boundary = EncapsulateChildren(boundary, transform.GetChild(i));
            }
        }

        return boundary;
    }
}
