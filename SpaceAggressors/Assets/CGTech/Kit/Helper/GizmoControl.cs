﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Anglia.CGTech.CKit.Helper
{
    public abstract class GizmoControl : MonoBehaviour
    {
        internal bool hideGizmos = false;
        protected abstract GizmoHelper.PartType PartType { get; }
        protected abstract void DrawGizmos();

        void OnDrawGizmos()
        {
            if (hideGizmos)
            {
                GizmoHelper.SetUnityGizmo(gameObject, GizmoHelper.PartType.Undefined);
            }
            else
            {
                OnDrawGizmosSelected();
            }
            
        }

        void OnDrawGizmosSelected()
        {
            GizmoHelper.SetUnityGizmo(gameObject, PartType);
            DrawGizmos();
        }

    }
}
