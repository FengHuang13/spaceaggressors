﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitVersion : MonoBehaviour {

    public const string KIT_VERSION = "1.62.5";
    public Rect location = new Rect(0, 0, 200, 25);
    private static KitVersion m_singleton = null;

    public static void CreateKitVersion()
    {
        if (m_singleton == null)
        {
            GameObject gob = new GameObject();
            gob.name = "Kit Helper";
            m_singleton = gob.AddComponent<KitVersion>();
        }
    }

    void OnGUI()
    {
        GUIContent content = new GUIContent("Construction Kit Version: " + KIT_VERSION);
        location.y = Screen.height - location.height;
        GUI.Label(location, content);
    }
}
