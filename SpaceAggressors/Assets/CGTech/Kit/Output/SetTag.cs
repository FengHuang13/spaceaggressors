﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anglia.CGTech.CKit.Output
{
    [AddComponentMenu("Construction Kit/Set Tag")]
    public class SetTag : GizmoControl
    {
        [Header("Inputs")]
        [SerializeField]
        private BooleanSource m_setNow;
        [SerializeField]
        private GameObjectSource m_objectToEdit;
        [SerializeField]
        private StringSource m_tagString;
        GameObject gob = null;
        void Update()
        {
            if (m_objectToEdit != null && m_setNow != null)
            {
                if (m_setNow.Fetch() && m_objectToEdit != null && m_tagString != null)
                {
                    gob = m_objectToEdit.Fetch();
                    if (gob != null)
                    {
                        gob.tag = m_tagString.Fetch();
                    }
                }
            }
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Effect;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_objectToEdit != null)
            {
                GizmoHelper.DrawArrow(transform.position, m_objectToEdit.transform.position, GizmoHelper.KitType.GameObject);
            }
            if (gob != null)
            {
                GizmoHelper.DrawArrow(gob.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
            if (m_setNow != null)
            {
                GizmoHelper.DrawArrow(m_setNow.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }
            if (m_tagString != null)
            {
                GizmoHelper.DrawArrow(m_tagString.transform.position, transform.position, GizmoHelper.KitType.String);
            }


            //UnityEditor.Handles.color = stringpair.color.HasValue ? stringpair.color.Value : Color.green;
            //UnityEditor.Handles.Label(transform.position, "Test");

        }
        #endregion
    }
}
