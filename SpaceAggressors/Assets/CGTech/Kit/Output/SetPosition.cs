﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anglia.CGTech.CKit.Output
{
    [AddComponentMenu("Construction Kit/Set Position")]
    public class SetPosition : GizmoControl
    {
        [Header("Inputs")]
        [SerializeField]
        private VectorSource m_targetPosition;
        [SerializeField]
        private GameObject m_ObjectToPlace;

        protected void OnEnable()
        {
            IconManager.SetIcon(gameObject, IconManager.LabelIcon.Red);
        }

        void Update()
        {
            if (m_ObjectToPlace != null && m_targetPosition != null)
            {
                m_ObjectToPlace.transform.position = m_targetPosition.Fetch();
            }
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Effect;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_ObjectToPlace != null)
            {
                GizmoHelper.DrawArrow(transform.position, m_ObjectToPlace.transform.position, GizmoHelper.KitType.GameObject);
            }
            if (m_targetPosition != null)
            {
                GizmoHelper.DrawArrow(m_targetPosition.transform.position, transform.position, GizmoHelper.KitType.Vector);
            }

        }
        #endregion
    }

}

