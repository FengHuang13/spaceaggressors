﻿using System;
using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Output
{
    [AddComponentMenu("Construction Kit/Create Instance")]
    public class CreateInstance : GameObjectSource
    {
        [Header("Inputs")]
        [SerializeField]
        private GameObject m_prefab;
        [SerializeField]
        private IntegerSource m_numberOfCopies;
        [SerializeField]
        private VectorSource m_location;
        [SerializeField]
        private BooleanSource m_trigger;
        [SerializeField]
        private BooleanBehaviour m_behaviour = BooleanBehaviour.OnTrue;
        [Header("Outputs")]
        [SerializeField]
        private GameObject m_currentValue = null;
        private bool lastTriggerValue = false;

        public override GameObject Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                

                bool currentTriggerValue = true;
                if (m_trigger != null)
                {
                    currentTriggerValue = m_trigger.Fetch();
                }

                int numberOfCopies = 1;
                if (m_numberOfCopies != null)
                    numberOfCopies = m_numberOfCopies.Fetch(true);

                if (m_prefab != null && currentTriggerValue)
                {
                    for (int i = 0; i < numberOfCopies; i++)
                    {
                        Vector3 location;
                        if (m_location == null)
                        {
                            location = transform.position;
                        }
                        else
                        {
                            location = new Vector3(m_location.Fetch(true).x, m_location.Fetch().y, 0);
                        }
                        switch (m_behaviour)
                        {
                            case BooleanBehaviour.OnTrue:
                                if (!lastTriggerValue && currentTriggerValue)
                                {
                                    m_currentValue = Instantiate(m_prefab, location, Quaternion.identity);
                                    m_currentValue.name = m_prefab.name;
                                }
                                break;
                            case BooleanBehaviour.OnFalse:
                                if (lastTriggerValue && !currentTriggerValue)
                                {
                                    m_currentValue = Instantiate(m_prefab, location, Quaternion.identity);
                                    m_currentValue.name = m_prefab.name;
                                }
                                break;
                            default:
                                m_currentValue = Instantiate(m_prefab, location, Quaternion.identity);
                                m_currentValue.name = m_prefab.name;
                                break;
                        }
                    }
                }
                //else if (m_behaviour != BooleanBehaviour.Once)
                //{
                //    m_currentValue = null;
                //}
                lastTriggerValue = currentTriggerValue;
            }
            return m_currentValue;
        }



        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Effect;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_trigger != null)
            {
                GizmoHelper.DrawArrow(m_trigger.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }
            if (m_location != null)
            {
                GizmoHelper.DrawArrow(m_location.transform.position, transform.position, GizmoHelper.KitType.Vector);
            }
            if (m_numberOfCopies != null)
            {
                GizmoHelper.DrawArrow(m_numberOfCopies.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }

        }
        #endregion
    }
}