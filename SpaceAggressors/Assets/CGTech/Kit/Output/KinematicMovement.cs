﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Anglia.CGTech.CKit.Output
{
    public class KinematicMovement : GizmoControl
    {
        [Header("Inputs")]
        [SerializeField]
        private VectorSource m_targetDirection;
        [SerializeField]
        private GameObject m_ObjectToMove;
        [Header("Process Options")]
        [SerializeField]
        private float m_speed = 1f;


        void Update()
        {
            if (m_ObjectToMove != null && m_targetDirection != null)
            {
                Vector2 displacement = m_targetDirection.Fetch() * m_speed * Time.deltaTime;
                m_ObjectToMove.transform.position += new Vector3(displacement.x, displacement.y, 0f);
            }
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Effect;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_ObjectToMove != null)
            {
                GizmoHelper.DrawArrow(transform.position, m_ObjectToMove.transform.position,
                    GizmoHelper.KitType.GameObject);
            }
            if (m_targetDirection != null)
            {
                GizmoHelper.DrawArrow(m_targetDirection.transform.position, transform.position,
                    GizmoHelper.KitType.Vector);
            }
        }
        #endregion
    }
}
