﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anglia.CGTech.CKit.Output
{
    [AddComponentMenu("Construction Kit/Destroy Game Object")]
    public class DestroyGameObject : GizmoControl
    {
        [Header("Inputs")]
        [SerializeField]
        private BooleanSource m_DestroyNow;
        [SerializeField]
        private GameObjectSource m_ObjectToDestroy;
        [SerializeField]
        [Range(0.001f, 100f)]
        private float m_delay = 1f/30f;
        GameObject gob = null;
        void Update()
        {
            if (m_ObjectToDestroy != null && m_DestroyNow != null)
            {
                if (m_DestroyNow.Fetch() && m_ObjectToDestroy != null)
                {
                    gob = m_ObjectToDestroy.Fetch();
                    if (gob != null)
                    {
                        Destroy(gob, m_delay);
                    }
                }
            }
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Effect;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_ObjectToDestroy != null)
            {
                GizmoHelper.DrawArrow(transform.position, m_ObjectToDestroy.transform.position, GizmoHelper.KitType.GameObject);
            }
            if (gob != null)
            {
                GizmoHelper.DrawArrow(gob.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
            if (m_DestroyNow != null)
            {
                GizmoHelper.DrawArrow(m_DestroyNow.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }



            //UnityEditor.Handles.color = stringpair.color.HasValue ? stringpair.color.Value : Color.green;
            //UnityEditor.Handles.Label(transform.position, "Test");

        }
        #endregion
    }
}
