﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Anglia.CGTech.CKit.UI
{
    public class DisplayInteger : GizmoControl
    {
        [Header("Inputs")]
        [SerializeField]
        private IntegerSource m_Source;
        
        [Header("Display Objects")]
        [SerializeField]
        private Text m_textField;

        void Update()
        {
            if (m_Source != null)
            {
                if (m_textField != null)
                {
                    m_textField.text = m_Source.Fetch().ToString("##0");
                }
            }

        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Effect;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_textField != null)
            {
                GizmoHelper.DrawArrow(transform.position, m_textField.transform.position, GizmoHelper.KitType.GameObject);
            }
            if (m_Source != null)
            {
                GizmoHelper.DrawArrow(m_Source.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }

        }
        #endregion
    }
}
