﻿using Anglia.CGTech.CKit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Anglia.CGTech.CKit.Helper;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Timer")]
    public class Timer : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        private BooleanSource m_Activation = null;
        [SerializeField]
        private BooleanSource m_Reset = null;
        [SerializeField]
        private FloatingPointSource m_Duration = null;
        [Header("Output")]
        [SerializeField]
        private float m_elapsedTime;
        [SerializeField]
        private bool m_currentValue = false;


        public override bool Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                bool isActive = false;
                if (m_Activation != null)
                    isActive = m_Activation.Fetch();
                else
                    isActive = true;
                if (isActive)
                    m_elapsedTime += Time.deltaTime;

                if (m_Duration != null)
                {


                    if (m_elapsedTime >= m_Duration.Fetch())
                    {
                        m_currentValue = true;
                    }
                    else
                    {
                        m_currentValue = false;
                    }
                    if (m_Reset != null)
                    {
                        if (m_Reset.Fetch())
                        {
                            if (m_elapsedTime >= m_Duration.Fetch())
                            {
                                m_elapsedTime = 0;
                            }
                        }
                    }
                }
            }
            return m_currentValue;
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Input;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_Activation != null)
                GizmoHelper.DrawArrow(m_Activation.transform.position, this.transform.position, GizmoHelper.KitType.Boolean);
            if (m_Duration != null)
                GizmoHelper.DrawArrow(m_Duration.transform.position, this.transform.position, GizmoHelper.KitType.Float);
            if (m_Reset != null)
                GizmoHelper.DrawArrow(m_Reset.transform.position, this.transform.position, GizmoHelper.KitType.Boolean);

        }
        #endregion
    }
}
