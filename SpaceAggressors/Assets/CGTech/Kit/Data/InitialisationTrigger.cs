﻿using Anglia.CGTech.CKit.Helper;
using Anglia.CGTech.CKit.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Anglia.CGTech.CKit.Data
{

    [AddComponentMenu("Construction Kit/Initialisation Trigger")]
    public class InitialisationTrigger : BooleanSource
    {
        [SerializeField]
        private BooleanBehaviour m_mode = BooleanBehaviour.OnTrue;
        [Header("Output")]
        [SerializeField]
        [Tooltip("The current value of the component.")]
        private bool m_currentValue = false;
        [SerializeField]
        private bool newValue = false;
        private bool m_isSet = false;
        public override bool Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;

                if (!m_isSet)
                {
                    m_isSet = true;
                    newValue = true;
                }
                else
                {
                    newValue = false;

                }
                switch (m_mode)
                {
                    case BooleanBehaviour.Continuous:
                        m_currentValue = newValue;
                        break;
                    case BooleanBehaviour.Once:
                        m_currentValue = m_currentValue || newValue;
                        break;
                    case BooleanBehaviour.OnTrue:
                        m_currentValue = m_currentValue == false && newValue == true;
                        break;
                    case BooleanBehaviour.OnFalse:
                        m_currentValue = m_currentValue == true && newValue == false;
                        break;
                    case BooleanBehaviour.Inverted:
                        m_currentValue = !newValue;
                        break;
                    default:
                        Debug.LogWarningFormat("Unexpected Boolean Mode {0} in InitialisationTrigger on {1}", m_mode.ToString(), gameObject.name);
                        break;
                }
            }
            newValue = false;
            return m_currentValue;
        }
        #region HFC
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }

        protected override void DrawGizmos()
        {

            //if (m_source != null)
            //{
            //    GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            //}

            //Gizmos.DrawIcon(transform.position, "FloatingPointStore.png", true);
        }
        #endregion
    }
}
