﻿using Anglia.CGTech.CKit.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Anglia.CGTech.CKit.Data
{
    public abstract class VectorSource : DataSource
    {
        public abstract Vector2 Fetch(bool force=false);
        
        void LateUpdate()
        {
#if UNITY_EDITOR
            Fetch();
#endif
        }
    }
}
