﻿using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anglia.CGTech.CKit.Data
{
    public abstract class IntegerSource : DataSource
    {
        internal int output;
        public abstract int Fetch(bool force = false);
       
        void LateUpdate()
        {
#if UNITY_EDITOR
            Fetch();
#endif
        }
    }

    
    public abstract class AdditionalIntData: MonoBehaviour
    {
        public abstract int GetData(string dataName);
    }
}

