﻿using Anglia.CGTech.CKit.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Anglia.CGTech.CKit.Data
{
    /// <summary>
    /// The Boolean Store component holds a boolean (true/false) value.  
    /// If an input source is provided, the store will hold the latest value output by that source.
    /// </summary>
    [AddComponentMenu("Construction Kit/Boolean Store")]
    public class BooleanStore : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        [Tooltip("If left as [none], the store will always have the Current Value set.  If linked, the value of the source will be held in this component.")]
        private BooleanSource m_source;
        [Header("Output")]
        [SerializeField]
        [Tooltip("The value set at design time or the current value of the source component.")]
        private bool m_currentValue = false;

        public override bool Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null)
                {
                    m_currentValue = m_source.Fetch();
                }
            }
            return m_currentValue;
        }

        internal override bool FindInSources(DataSource dataObject)
        {
            return CheckForSource(dataObject, m_source);
        }



        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }
        }
    }
}
