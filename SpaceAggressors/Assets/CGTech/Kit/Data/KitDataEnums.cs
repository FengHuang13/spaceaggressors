﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anglia.CGTech.CKit.Data
{
    

    public enum BooleanBehaviour
    {
        Undefined = 0,
        OnTrue,
        Continuous,
        OnFalse,
        Once,
        Inverted
    }

    public enum BooleanLogicMode
    {
        Undefined = 0,
        And,
        Or,
        Xor,
        Not
    }

    public enum GameObjectKeyString
    {
        Undefined = 0,
        Tag,
        Name
    }

    public enum AxesOfComparison
    {
        Undefined = 0,
        X,
        Y,
        Distance
    }

    public enum FloatIntConversion
    {
        Undefined = 0,
        Floor,
        Ceiling,
        Round
    }

    public enum Comparisons
    {
        Undefined = 0,
        ALessThanB,
        AGreaterThanB,
        AEqualsB,
        AApproximatelyB
    }

}
