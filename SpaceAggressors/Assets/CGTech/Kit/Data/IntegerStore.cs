﻿using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Data
{
    [AddComponentMenu("Construction Kit/Integer Store")]

    public class IntegerStore : IntegerSource
    {
        [Header("Inputs")]
        [SerializeField]
        [Tooltip("Set to None to use Current Value as a constant.")]
        protected IntegerSource m_source;
        [SerializeField]
        private bool m_ignoreSourceAtStart = false;
        [Header("Output")]
        [SerializeField]
        protected int m_currentValue;


        public int Value
        {
            get
            {
                return m_currentValue;
            }
            set
            {
                m_currentValue = value;
            }
        }


        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }
        }

        public override int Fetch(bool force=false)
        {
            //bool isInChain = m_source.IsInChain(this);
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null && !m_ignoreSourceAtStart)
                {
                    m_currentValue = m_source.Fetch();
                }
                else
                {
                    m_ignoreSourceAtStart = false;
                }
            }
            return m_currentValue; 
        }

        internal override bool FindInSources(DataSource dataObject)
        {
            return CheckForSource(dataObject,m_source);
        }
    }
}