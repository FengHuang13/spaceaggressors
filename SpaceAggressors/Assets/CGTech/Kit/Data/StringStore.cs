﻿using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Data
{
    [AddComponentMenu("Construction Kit/String Store")]
    public class StringStore : StringSource
    {
        [Header("Inputs")]
        [SerializeField]
        private StringSource m_source;
        [Header("Output")]
        [SerializeField]
        private string m_currentValue = "";
        public override string Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null)
                {
                    m_currentValue = m_source.Fetch();
                }
            }
            return m_currentValue;
        }

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }

            //Gizmos.DrawIcon(transform.position, "FloatingPointStore.png", true);
        }
    }
}
