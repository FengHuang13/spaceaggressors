﻿using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Data
{
    [AddComponentMenu("Construction Kit/Integer Buffer")]

    public class IntegerBuffer : IntegerSource
    {
        [Header("Inputs")]
        [SerializeField]
        [Tooltip("Set to None to use Current Value as a constant.")]
        protected IntegerSource m_source;
        [Header("Output")]
        [SerializeField]
        protected int m_currentValue;
        private int m_lastValue;

        public int Value
        {
            get
            {
                return m_currentValue;
            }
            set
            {
                m_currentValue = value;
            }
        }


        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }
        }

        public override int Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null)
                {
                    m_currentValue = m_lastValue;
                    m_lastValue = m_source.Fetch();
                }
            }
            return m_currentValue;
        }
    }
}