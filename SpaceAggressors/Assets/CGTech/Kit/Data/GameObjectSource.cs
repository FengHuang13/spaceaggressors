﻿using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Anglia.CGTech.CKit.Data
{
    public abstract class GameObjectSource : DataSource
    {

        public abstract GameObject Fetch(bool force=false);
        
        void LateUpdate()
        {
#if UNITY_EDITOR
            Fetch();
#endif
        }
    }
}
