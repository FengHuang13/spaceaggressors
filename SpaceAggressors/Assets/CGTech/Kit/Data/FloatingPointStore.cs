﻿using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Data
{
    [AddComponentMenu("Construction Kit/Floating Point Store")]
    public class FloatingPointStore : FloatingPointSource
    {
        [Header("Inputs")]
        [SerializeField]
        [Tooltip("Set to None to use Current Value as a constant.")]
        protected FloatingPointSource m_source;
        [Header("Output")]
        [SerializeField]
        protected float m_currentValue;


        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Float);
            }

        }

        public override float Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null)
                {
                    m_currentValue = m_source.Fetch();
                }
            }
            return m_currentValue;
        }
    }
}
