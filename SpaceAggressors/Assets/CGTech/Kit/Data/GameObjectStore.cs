﻿using Anglia.CGTech.CKit.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Anglia.CGTech.CKit.Data
{
    [AddComponentMenu("Construction Kit/Game Object Store")]
    public class GameObjectStore : GameObjectSource
    {
        [Header("Inputs")]
        [SerializeField]
        private GameObjectSource m_source;
        [Header("Output")]
        [SerializeField]
        private GameObject m_currentValue = null;
        public override GameObject Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null)
                {
                    m_currentValue = m_source.Fetch();
                }
            }
            return m_currentValue;
        }
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }
        protected override void DrawGizmos()
        {
            
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }

            //Gizmos.DrawIcon(transform.position, "FloatingPointStore.png", true);
        }
    }
}
