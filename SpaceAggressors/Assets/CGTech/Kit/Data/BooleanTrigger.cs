﻿using Anglia.CGTech.CKit.Helper;
using Anglia.CGTech.CKit.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Anglia.CGTech.CKit.Data
{
    [AddComponentMenu("Construction Kit/Boolean Trigger")]
    public class BooleanTrigger : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        [Tooltip("")]
        private BooleanSource m_source;
        [SerializeField]
        private BooleanBehaviour m_mode = BooleanBehaviour.OnTrue;
        [Header("Output")]
        [SerializeField]
        [Tooltip("The value set at design time or the current value of the source component.")]
        private bool m_currentValue = false;
        [SerializeField]
        private bool newValue = false;
        private bool lastValue = false;

        public override bool Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                
                if (m_source != null)
                {
                    newValue = m_source.Fetch();
                }

                switch (m_mode)
                {
                    case BooleanBehaviour.Inverted:
                        m_currentValue = !newValue;
                        break;
                    case BooleanBehaviour.Continuous:
                        m_currentValue = newValue;
                        break;
                    case BooleanBehaviour.Once:
                        m_currentValue = m_currentValue || newValue;
                        break;
                    case BooleanBehaviour.OnTrue:
                        m_currentValue = m_currentValue == false && newValue == true && lastValue != newValue;
                        break;
                    case BooleanBehaviour.OnFalse:
                        m_currentValue = m_currentValue == true && newValue == false&& lastValue != newValue;
                        break;

                    default:
                        Debug.LogWarningFormat("Unexpected Boolean Mode {0} in InitialisationTrigger on {1}", m_mode.ToString(), gameObject.name);
                        break;

                }
            }
            lastValue = newValue;
            //newValue = false;
            return m_currentValue;
        }
        #region HFC
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }

        protected override void DrawGizmos()
        {
            
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }

            //Gizmos.DrawIcon(transform.position, "FloatingPointStore.png", true);
        }
        #endregion
    }
}
