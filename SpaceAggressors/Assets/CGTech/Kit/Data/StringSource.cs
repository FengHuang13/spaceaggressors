﻿using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Anglia.CGTech.CKit.Data
{
    public abstract class StringSource : DataSource
    {

        public abstract string Fetch(bool force=false);

        void LateUpdate()
        {
#if UNITY_EDITOR
            Fetch();
#endif
        }
    }
}