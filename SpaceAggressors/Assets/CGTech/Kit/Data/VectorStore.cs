﻿using Anglia.CGTech.CKit.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anglia.CGTech.CKit.Data
{
    [AddComponentMenu("Construction Kit/Vector Store")]

    public class VectorStore : VectorSource
    {
        [Header("Inputs")]
        [SerializeField]
        private VectorSource m_source;
        //[Header("Options")]
        //[SerializeField]
        //private bool m_updateContinuously = true;
        [Header("Output")]
        [SerializeField]
        private Vector2 m_currentValue;


        public Vector2 CurrentValue
        {
            get
            {
                return m_currentValue;
            }

            set
            {
                m_currentValue = value;
            }
        }

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Storage;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Vector);
            }
        }

        public override Vector2 Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null)
                {
                    m_currentValue = m_source.Fetch();
                }
                
            }
            
            return m_currentValue; 
        }

        

    }
}
