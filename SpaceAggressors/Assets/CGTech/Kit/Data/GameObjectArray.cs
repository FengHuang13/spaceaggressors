﻿using UnityEngine;
using Anglia.CGTech.CKit.Helper;
using System;

namespace Anglia.CGTech.CKit.Data
{
    public abstract class GameObjectArraySource : AdditionalIntData
    {
        public const string LENGTH_NAME = "length";
        public abstract GameObject[] Fetch();
        internal bool m_calculatedThisFrame;
        void Update()
        {
            m_calculatedThisFrame = false;
        }

        void LateUpdate()
        {
#if UNITY_EDITOR
            Fetch();
#endif
        }

        

    }



    public class GameObjectArray : GameObjectArraySource
    {
        [Header("Inputs")]
        [SerializeField]
        private GameObject[] m_sourceList;
        private GameObject[] m_currentValue;

        public override GameObject[] Fetch()
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                m_currentValue = m_sourceList;
            }
            return m_currentValue;
        }

        void OnDrawGizmos()
        {
            GizmoHelper.SetUnityGizmo(gameObject, GizmoHelper.PartType.Storage);

            for (int i = 0; i < m_sourceList.Length; i++)
            {
                GameObject currentItem = m_sourceList[i];
                if (currentItem != null)
                {
                    GizmoHelper.DrawArrow(currentItem.transform.position, transform.position, GizmoHelper.KitType.GameObject);
                }
            }
        }

        public override int GetData(string dataName)
        {
            int result = 0;
            switch (dataName.ToLower())
            {
                case LENGTH_NAME:
                    if (m_currentValue != null)
                        return m_currentValue.Length;
                    break;
            }
            return result;
        }
    }
}