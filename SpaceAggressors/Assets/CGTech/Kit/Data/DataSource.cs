﻿using Anglia.CGTech.CKit.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anglia.CGTech.CKit.Data
{
    public abstract class DataSource : GizmoControl
    {

        protected bool m_calculatedThisFrame;

        protected void OnEnable()
        {
            KitVersion.CreateKitVersion();
        }

        protected void Update()
        {
            m_calculatedThisFrame = false;
        }

        internal bool IsInChain(DataSource dataObject)
        {
            bool result = false;
            result = FindInSources(dataObject);
            return result;
        }

        internal virtual bool FindInSources(DataSource dataObject) { return false; }

        protected bool CheckForSource(DataSource dataObject, DataSource m_source)
        {
            bool result = false;
            if (m_source != null)
            {
                if (m_source == dataObject)
                    result = true;
                else
                    result = m_source.FindInSources(dataObject);
            }
            return result;
        }

        protected bool CheckForSource(DataSource dataObject, DataSource[] m_source)
        {
            bool result = false;
            if (m_source != null)
            {
                for (int i = 0; i > m_source.Length; i++)
                {
                    if (m_source[i] == dataObject)
                        result = true;
                    else
                        result = m_source[i].FindInSources(dataObject);
                }
            }
            return result;
        }


    }
}
