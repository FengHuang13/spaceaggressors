﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Loop")]
    public class Loop : AdditionalTriggerData
    {
        [Header("Inputs")]
        [SerializeField]
        private IntegerSource m_fromValue;
        [SerializeField]
        private IntegerSource m_untilValue;
        [SerializeField]
        private BooleanSource m_incrementNow;
        [SerializeField]
        private BooleanSource m_reset;
        //[SerializeField]
        //private bool m_repeat = true;
        [Header ("Outputs")]
        [SerializeField]
        private int m_currentValue = int.MinValue;
        private bool m_changeTrigger = false;
        private int delay = 0;
        public override int Fetch(bool force = false)
        {
            

            if (!m_calculatedThisFrame)
            {
                
                m_changeTrigger = false;
                m_calculatedThisFrame = true;
                int fromValue=0, toValue=0;
                if (m_fromValue != null)
                    fromValue = m_fromValue.Fetch();
                if (m_untilValue != null)
                    toValue = m_untilValue.Fetch();

                if (m_fromValue != null && m_untilValue != null )
                {
                    if (m_currentValue == int.MinValue ||
                        (m_reset != null && m_reset.Fetch()))
                    {
                        m_currentValue = fromValue;
                        m_changeTrigger = true;
                    }
                    else if (m_incrementNow == null || m_incrementNow.Fetch())
                    {
                        if ((m_currentValue + 1) < toValue)
                        {
                            m_currentValue++;
                            m_changeTrigger = true;
                        }

                    }
                }
            }
            return m_currentValue;
        }
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_fromValue != null)
            {
                GizmoHelper.DrawArrow(m_fromValue.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }
            if (m_untilValue != null)
            {
                GizmoHelper.DrawArrow(m_untilValue.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }
            if (m_incrementNow != null)
            {
                GizmoHelper.DrawArrow(m_incrementNow.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }
            if (m_reset != null)
            {
                GizmoHelper.DrawArrow(m_reset.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }
        }

        public override bool GetTrigger()
        {
            Fetch();
            return m_changeTrigger;
        }
    }
}