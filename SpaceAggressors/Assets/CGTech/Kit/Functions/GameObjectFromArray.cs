﻿using Anglia.CGTech.CKit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Anglia.CGTech.CKit.Helper;

namespace Anglia.CGTech.CKit.Functions
{
    public class GameObjectFromArray : GameObjectSource
    {
        [Header("Inputs")]
        [SerializeField]
        private GameObjectArraySource m_sourceArray;
        [SerializeField]
        private IntegerSource m_selectionIndex;
        [Header("Output")]
        [SerializeField]
        private GameObject m_currentValue;

        public override GameObject Fetch(bool force=false)
        {
            if (m_sourceArray != null)
            {
                GameObject[] array = m_sourceArray.Fetch();
                if (array != null && array.Length > 0)
                {
                    if (m_selectionIndex == null)
                    {
                        m_currentValue = array[0];
                    }
                    else
                    {
                        int index = m_selectionIndex.Fetch();
                        if (index <0)
                        {
                            m_currentValue = array[0];
                        }
                        else if (index < array.Length)
                        {
                            m_currentValue = array[index];
                        }
                        else
                        {
                            m_currentValue = array[array.Length - 1];
                        }
                    }
                }
            }
            return m_currentValue;
        }
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_sourceArray != null)
            {
                GizmoHelper.DrawArrow(m_sourceArray.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
            if (m_currentValue != null)
            {
                GizmoHelper.DrawArrow(m_currentValue.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
            if (m_selectionIndex != null)
            {
                GizmoHelper.DrawArrow(m_selectionIndex.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }
        }
    }
}