﻿using Anglia.CGTech.CKit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Anglia.CGTech.CKit.Helper;
using Anglia.CGTech.CKit.Helper;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Compare Strings")]
    public class CompareStrings : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        private StringSource m_firstString;
        [SerializeField]
        private StringSource m_secondString;
        [Header("Output")]
        [SerializeField]
        private bool m_currentValue;

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }

        public override bool Fetch(bool force=false)
        {
            m_currentValue = false;
            if (m_firstString != null && m_secondString != null)
            {
                string firstString = m_firstString.Fetch();
                if (firstString != null)
                {
                    m_currentValue = firstString.Equals(m_secondString.Fetch());
                }
            }
            return m_currentValue;
        }

        protected override void DrawGizmos()
        {
            if (m_firstString != null)
            {
                GizmoHelper.DrawArrow(m_firstString.transform.position, transform.position, GizmoHelper.KitType.String);
            }

            if (m_secondString != null)
            {
                GizmoHelper.DrawArrow(m_secondString.transform.position, transform.position, GizmoHelper.KitType.String);
            }


        }

       
    }
}
