﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Integer to Float Conversion")]
    public class IntToFloat : FloatingPointSource
    {
        [Header("Inputs")]
        public IntegerSource m_Input;

        [Header("Output")]
        [SerializeField]
        private float m_currentValue;

        public override float Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                m_currentValue = Calculate();
            }
            return m_currentValue;
        }

        protected float Calculate()
        {
            float result = 0;
            bool wasCalculated = false;
            if (m_Input != null)
            {

                result = (float)m_Input.Fetch();
                wasCalculated = true;

            }

            if (wasCalculated)
            {
                m_currentValue = result;
            }
            
            return m_currentValue;
        }



        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_Input != null)
            {
                GizmoHelper.DrawArrow(m_Input.transform.position, transform.position, GizmoHelper.KitType.Float);
            }
        }
        #endregion

    }
}