﻿using System;
using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Invert Floating Point")]
    public class InvertFloatingPoint : FloatingPointSource
    {
        [Header("Inputs")]
        public FloatingPointSource m_source;
        [Header("Output")]
        [SerializeField]
        private float m_currentValue;




        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Float);
            }

        }

        public override float Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                m_currentValue = Calculate();
            }
            return m_currentValue;
        }
        #endregion

        protected float Calculate()
        {
            float result = 0f;

            if (m_source != null)
            {
                result = 1f / m_source.Fetch();
            }

            m_currentValue = result;
            return result;
        }

    }
}

