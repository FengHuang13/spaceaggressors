﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using Anglia.CGTech.CKit.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.CGTech.Kit.Functions
{
    public class IterationTrigger : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        private AdditionalTriggerData m_source;
        [Header("Output")]
        [SerializeField]
        private bool m_currentValue;



        public override bool Fetch(bool force=false)
        {
            if (m_source != null)
            {
                m_currentValue = m_source.GetTrigger();
            }
            return m_currentValue;
        }

        #region HFC
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.gameObject.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }
        }
        #endregion

    }
}

