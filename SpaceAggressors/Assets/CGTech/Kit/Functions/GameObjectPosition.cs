﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.CGTech.Kit.Functions
{
    public class GameObjectPosition : VectorSource
    {
        [Header("Input")]
        [SerializeField]
        private GameObjectSource m_source;
        [Header("Output")]
        [SerializeField]
        private Vector2 m_currentValue;

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }

        public override Vector2 Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null)
                {
                    GameObject gob = m_source.Fetch();
                    m_currentValue = new Vector2(gob.transform.position.x, gob.transform.position.y);
                }
            }
            return m_currentValue;
        }

        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
            GizmoHelper.DrawArrow(new Vector3(m_currentValue.x, m_currentValue.y, 0f), transform.position, GizmoHelper.KitType.Vector);
        }


    }
}
