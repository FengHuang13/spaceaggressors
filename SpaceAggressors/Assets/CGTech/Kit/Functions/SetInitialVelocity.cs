﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Output
{
    [AddComponentMenu("Construction Kit/Set Initial Velocity")]
    public class SetInitialVelocity : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        private VectorSource m_velocity;
        [SerializeField]
        private Rigidbody2D m_target;
        [Header("Output")]
        [SerializeField]
        private Vector2 m_currentVelocity = Vector2.zero;
        [SerializeField]
        private bool m_currentValue = false;



        public override bool Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame && !m_currentValue)
            {
                m_calculatedThisFrame = true;
                if (m_velocity != null && m_target != null)
                {
                    Vector2 newValue = m_velocity.Fetch();
                    if (newValue != m_currentVelocity)
                    {
                        m_currentVelocity = newValue;
                        m_target.velocity = m_currentVelocity;
                        m_currentValue = true;
                    }
                }
            }
            return m_currentValue;
        }

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Effect;
            }
        }

        protected override void DrawGizmos()
        {

            if (m_target != null)
            {
                GizmoHelper.DrawArrow(transform.position, m_target.transform.position, GizmoHelper.KitType.GameObject);
            }
            if (m_velocity != null)
            {
                GizmoHelper.DrawArrow(m_velocity.transform.position, transform.position, GizmoHelper.KitType.Vector);
            }

        }


        #endregion
    }
}
