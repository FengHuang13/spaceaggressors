﻿using System;
using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;
namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Scale Floating Point")]
    public class ScaleFloatingPoint : FloatingPointSource
    {
        [Header("Inputs")]
        public FloatingPointSource[] m_sources;

        [Header("Output")]
        [SerializeField]
        private float m_currentValue;





        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }
        protected override void DrawGizmos()
        {
            for (int i = 0; i < m_sources.Length; i++)
            {
                if (m_sources[i] != null)
                {
                    GizmoHelper.DrawArrow(m_sources[i].transform.position, transform.position, GizmoHelper.KitType.Float);
                }
            }

        }

        public override float Fetch(bool force = false)
        {
            if (!m_calculatedThisFrame || force)
            {
                m_calculatedThisFrame = true;
                {
                    float result = 1f;
                    bool wasCalculated = false;
                    for (int i = 0; i < m_sources.Length; i++)
                    {
                        if (m_sources[i] != null)
                        {
                            result = result * m_sources[i].Fetch(force);
                            wasCalculated = true;
                        }
                    }

                    if (wasCalculated)
                    {
                        m_currentValue = result;
                    }
                    else
                    {
                        m_currentValue = 0f;
                    }
                }
            }
            return m_currentValue;
        }



        #endregion
    }
}