﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Object Exists")]
    public class ObjectExists : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        private GameObjectSource m_source;
        [SerializeField]
        private BooleanBehaviour m_mode = BooleanBehaviour.Once;
        [Header("Output")]
        [SerializeField]
        private bool m_currentValue;
        private bool m_lastValue;

        public override bool Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                bool newValue = false;
                if (m_source != null)
                {
                    if (m_source.Fetch() != null)
                        newValue = true;
                }
                else
                {
                    newValue = true;
                }

                switch (m_mode)
                {
                    case BooleanBehaviour.Undefined:
                    case BooleanBehaviour.Continuous:
                        m_currentValue = newValue;
                        break;
                    case BooleanBehaviour.OnTrue:
                        m_currentValue = newValue == true && m_lastValue == false; 
                        break;
                    case BooleanBehaviour.OnFalse:
                        m_currentValue = newValue == false && m_lastValue == true;
                        break;
                    case BooleanBehaviour.Once:
                        m_currentValue = newValue == true || m_lastValue == true;
                        break;
                    case BooleanBehaviour.Inverted:
                        m_currentValue = !newValue;
                        break;
                }

            }
            m_lastValue = m_currentValue;
            return m_currentValue;
        }

        #region Helpful functionality code, it is not essential to understand at level 4

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }

        }

        #endregion
    }
}
