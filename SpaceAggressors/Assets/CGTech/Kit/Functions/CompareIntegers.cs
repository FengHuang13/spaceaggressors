﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Compare Integers")]
    public class CompareIntegers : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        private IntegerSource m_valueA;
        [SerializeField]
        private IntegerSource m_valueB;
        public Comparisons m_mode = Comparisons.ALessThanB;
        [Header("Output")]
        [SerializeField]
        private bool m_currentValue;

        public override bool Fetch(bool force=false)
        {
            m_currentValue = false;
            if (m_valueA != null && m_valueB != null)
            {
                float a = m_valueA.Fetch();
                float b = m_valueB.Fetch();
                switch (m_mode)
                {
                    case Comparisons.ALessThanB:
                        m_currentValue = a < b;
                        break;
                    case Comparisons.AEqualsB:
                        m_currentValue = a.Equals(b);
                        break;
                    case Comparisons.AGreaterThanB:
                        m_currentValue = a > b;
                        break;
                    case Comparisons.AApproximatelyB:
                        m_currentValue = Mathf.Approximately(a, b);
                        break;

                }
            }
            return m_currentValue;
        }

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_valueA != null)
            {
                GizmoHelper.DrawArrow(m_valueA.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }

            if (m_valueB != null)
            {
                GizmoHelper.DrawArrow(m_valueB.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }


        }
    }
}
