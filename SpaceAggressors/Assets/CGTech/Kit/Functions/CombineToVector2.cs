﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Anglia.CGTech.CKit.Functions
{
    public class CombineToVector2 : VectorSource
    {
        [Header("Inputs")]
        [SerializeField]
        private FloatingPointSource m_x;
        [SerializeField]
        private FloatingPointSource m_y;
        public bool normalise = false;
        [Header("Output")]
        [SerializeField]
        private Vector2 m_currentValue;

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_x != null)
            {
                GizmoHelper.DrawArrow(m_x.transform.position, transform.position, GizmoHelper.KitType.Float);
            }
            if (m_y != null)
            {
                GizmoHelper.DrawArrow(m_y.transform.position, transform.position, GizmoHelper.KitType.Float);
            }

        }

        public override Vector2 Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame || force)
            {
                m_calculatedThisFrame = true;

                {
                    float countSetInputs = 0;
                    if (m_x != null)
                        countSetInputs++;
                    if (m_y != null)
                        countSetInputs++;

                    if (countSetInputs == 2)
                    {
                        m_currentValue = new Vector2(m_x.Fetch(force), m_y.Fetch(force));
                    }
                    else if (countSetInputs > 0)
                    {
                        Debug.LogWarningFormat("Either {1} or none of the axis inputs to Combine to Vector on {0} must be linked", gameObject.name, 2);
                    }
                    if (normalise)
                        m_currentValue.Normalize();
                    
                }
            }
            Debug.LogFormat("{1} Generated: {0}", m_currentValue.ToString(), gameObject.name);
            return m_currentValue;
        }

        internal override bool FindInSources(DataSource dataObject)
        {
            return CheckForSource(dataObject, m_x) || CheckForSource(dataObject, m_y);
        }


        

    }
}