﻿using System;
using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;
using Anglia.CGTech.CKit.Helper;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Floating to Int Conversion")]
    public class FloatToInt : IntegerSource
    {
        [Header("Inputs")]
        public FloatingPointSource m_Input;
        public FloatIntConversion m_mode = FloatIntConversion.Round;

        [Header("Output")]
        [SerializeField]
        private int m_currentValue;

        public override int Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                output = Calculate();
            }
            return output;
        }

        protected int Calculate()
        {
            int result = 0;
            bool wasCalculated = false;
            if (m_Input != null)
            {
                switch (m_mode)
                {
                    case FloatIntConversion.Floor:
                        result = Mathf.FloorToInt(m_Input.Fetch());
                        break;
                    case FloatIntConversion.Ceiling:
                        result = Mathf.CeilToInt(m_Input.Fetch());
                        break;
                    case FloatIntConversion.Round:
                        result = Mathf.RoundToInt(m_Input.Fetch());
                        break;

                }
                wasCalculated = true;

            }

            if (wasCalculated)
            {
                m_currentValue = output = result;
            }
            else
            {
                m_currentValue = output;
            }
            return m_currentValue;
        }



        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }

        protected override void DrawGizmos()
        {
            if (m_Input != null)
            {
                GizmoHelper.DrawArrow(m_Input.transform.position, transform.position, GizmoHelper.KitType.Float);
            }
            //if (m_upperBound != null)
            //{
            //    GizmoHelper.DrawArrow(m_upperBound.transform.position, transform.position, GizmoHelper.KitType.Float);
            //}
            //if (m_lowerBound != null)
            //{
            //    GizmoHelper.DrawArrow(m_lowerBound.transform.position, transform.position, GizmoHelper.KitType.Float);
            //}


        }



        #endregion
    }
}