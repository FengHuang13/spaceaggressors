﻿using Anglia.CGTech.CKit.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;

namespace Anglia.CGTech.Kit.Functions
{
    public class DoTrigger : BooleanSource
    {
        [Header("Inputs")]
        [SerializeField]
        private BooleanSource m_do;
        [SerializeField]
        private BooleanSource m_reset;

        public override bool Fetch(bool force=false)
        {
            if (m_calculatedThisFrame == false)
            {
                m_calculatedThisFrame = true;
                if (m_do != null)
                {
                    if (m_do.Fetch())
                    {

                    }
                }
            }
            return false;
        }


        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }



        protected override void DrawGizmos()
        {
            //if (m_fromValue != null)
            //{
            //    GizmoHelper.DrawArrow(m_fromValue.transform.position, transform.position, GizmoHelper.KitType.Integer);
            //}
            //if (m_untilValue != null)
            //{
            //    GizmoHelper.DrawArrow(m_untilValue.transform.position, transform.position, GizmoHelper.KitType.Integer);
            //}
            //if (m_incrementNow != null)
            //{
            //    GizmoHelper.DrawArrow(m_incrementNow.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            //}
            //if (m_reset != null)
            //{
            //    GizmoHelper.DrawArrow(m_reset.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            //}
        }
    }
}
