﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.CGTech.Kit.Functions
{
    public class ExtractIntegerStore : IntegerSource
    {
        [Header("Input")]
        [SerializeField]
        private GameObjectSource m_source;
        [Header("Output")]
        [SerializeField]
        private int m_currentValue;

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }

        public override int Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                if (m_source != null)
                {
                    GameObject gob = m_source.Fetch();
                    IntegerSource source = gob.GetComponent<IntegerSource>();
                    if (source != null)
                        m_currentValue = source.Fetch();
                }
            }
            return m_currentValue;
        }

        protected override void DrawGizmos()
        {
            if (m_source != null)
            {
                GizmoHelper.DrawArrow(m_source.transform.position, transform.position, GizmoHelper.KitType.GameObject);
            }
        }


    }
}
