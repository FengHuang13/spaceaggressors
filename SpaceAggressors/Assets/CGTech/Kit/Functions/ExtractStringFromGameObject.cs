﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Anglia.CGTech.CKit.Helper;

namespace Anglia.CGTech.CKit.Input
{
    [AddComponentMenu("Construction Kit/Extract String From Game Object")]
    public class ExtractStringFromGameObject : StringSource
    {

        [Header("Input")]
        [SerializeField]
        private GameObjectSource m_target;
        [SerializeField]
        private GameObjectKeyString m_mode = GameObjectKeyString.Tag;
        [Header("Output")]
        [SerializeField]
        private string m_currentValue;




        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_target != null)
            {
                GizmoHelper.DrawArrow(m_target.transform.position, transform.position, GizmoHelper.KitType.String);
            }
        }

        public override string Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                GameObject currentTarget = m_target.Fetch();
                if (m_target != null && currentTarget != null)
                {
                    switch (m_mode)
                    {
                        case GameObjectKeyString.Tag:
                            m_currentValue = currentTarget.tag;
                            Debug.LogFormat("Extracted tag from {0} is {1}", currentTarget.name, m_currentValue);
                            break;
                        case GameObjectKeyString.Name:
                            m_currentValue = currentTarget.name;
                            break;
                        default:
                            m_currentValue = "";
                            break;
                    }

                }

            }
            return m_currentValue;
        }
    }
}