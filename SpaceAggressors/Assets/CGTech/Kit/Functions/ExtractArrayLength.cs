﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.CGTech.Kit.Functions
{
    public class ExtractArrayLength : IntegerSource
    {
        [Header("Inputs")]
        [SerializeField]
        private AdditionalIntData m_sourceArray;
        [Header("Output")]
        [SerializeField]
        private int m_currentValue;

        public override int Fetch(bool force=false)
        {
            if (m_sourceArray != null)
            {
                m_currentValue = m_sourceArray.GetData("length");
            }
            return m_currentValue;
        }
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Discovery;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_sourceArray != null)
            {
                GizmoHelper.DrawArrow(m_sourceArray.gameObject.transform.position, transform.position, GizmoHelper.KitType.Integer);
            }
        }
    }
}
