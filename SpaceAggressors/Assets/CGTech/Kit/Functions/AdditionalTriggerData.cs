﻿using Anglia.CGTech.CKit.Data;
using UnityEngine;

namespace Anglia.CGTech.CKit.Data
{
    public abstract class AdditionalTriggerData:IntegerSource
    {
        public abstract bool GetTrigger();
    }
}