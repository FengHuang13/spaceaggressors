﻿using System;
using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using UnityEngine;
namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Scale Vector")]
    public class ScaleVector : VectorSource
    {
        [Header("Inputs")]
        public FloatingPointSource m_scaleFactor;
        public VectorSource m_vectorSource;

        [Header("Output")]
        [SerializeField]
        private Vector2 m_currentValue;

        #region Helpful functionality code, it is not essential to understand at level 4
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }

        
        protected override void DrawGizmos()
        {
            if (m_scaleFactor != null && m_vectorSource != null)
            {
                GizmoHelper.DrawArrow(m_scaleFactor.transform.position, transform.position, GizmoHelper.KitType.Float);
                GizmoHelper.DrawArrow(m_vectorSource.transform.position, transform.position, GizmoHelper.KitType.Vector);
            }
           
        }
        #endregion

        public override Vector2 Fetch(bool force=false)
        {
           if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                m_currentValue = Calculate();
            }
            return m_currentValue;
        }

        protected  Vector2 Calculate()
        {
            Vector2 result = Vector2.zero;
            bool wasCalculated = false;

            if (m_scaleFactor != null && m_vectorSource != null)
            {
                result = m_vectorSource.Fetch() * m_scaleFactor.Fetch();
                wasCalculated = true;
            }


            if (wasCalculated)
            {
                m_currentValue = result;
            }
            else
            {
                m_currentValue = Vector2.zero;
            }
            return m_currentValue;
        }


        
    }
}