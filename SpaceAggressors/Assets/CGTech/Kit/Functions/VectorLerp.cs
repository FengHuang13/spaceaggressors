﻿using Anglia.CGTech.CKit.Data;
using Anglia.CGTech.CKit.Helper;
using System;
using UnityEngine;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Vector Lerp")]
    public class VectorLerp: VectorSource
    {
        [Header("Inputs")]
        [Tooltip("Lower boundary point")]
        public VectorSource guidePoint0;

        [Tooltip("Upper boundary point")]
        public VectorSource guidePoint1;

        [Tooltip("Target factor")]
        public FloatingPointSource targetFactor;

        [Header ("Process Options")]
        
        [SerializeField]
        [Range(0.001f, 3f)]
        private float speed = 0.5f;

        [Header ("Output")]
        [SerializeField]
        [Range(0f, 1f)]
        private float m_currentPositionValue01 = 0.5f;

        [SerializeField]
        private Vector2 m_currentValue;

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }


        #region Helpful functionality code, it is not essential to understand at level 4
        protected override void DrawGizmos()
        {
            if (targetFactor != null)
            {
                GizmoHelper.DrawArrow(targetFactor.transform.position, transform.position, GizmoHelper.KitType.Float);
            }
            if (guidePoint0 != null)
            {
                GizmoHelper.DrawArrow(guidePoint0.transform.position, transform.position, GizmoHelper.KitType.Vector);
            }
            if (guidePoint1 != null)
            {
                GizmoHelper.DrawArrow(guidePoint1.transform.position, transform.position, GizmoHelper.KitType.Vector);
            }
        }

        public override Vector2 Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                m_calculatedThisFrame = true;
                
                m_currentValue = Calculate();
            }
            return m_currentValue;
        }

        protected  Vector2 Calculate()
        {
            if (guidePoint0 != null && guidePoint1 != null && targetFactor != null)
            {
                //Calculate current position based on input, speed setting, and frame duration.
                float deviation = (targetFactor.Fetch() - m_currentPositionValue01);
                //if (Mathf.Approximately(deviation, 0))
                {
                    m_currentPositionValue01 = targetFactor.Fetch();
                }
                //else
                //{
                //    m_currentPositionValue01 += deviation * speed * Time.deltaTime;
                //}
                //Limit current position to 0-1 range
                m_currentPositionValue01 = Mathf.Clamp01(m_currentPositionValue01);
                //Reposition gameObject's transform to correct position.
                m_currentValue = Vector2.Lerp(guidePoint0.Fetch(), guidePoint1.Fetch(), m_currentPositionValue01);
            }
            else
            {
                Debug.LogWarningFormat("One or more inputs is missing from VectorLerp component on {0}", gameObject.name);
            }
            return m_currentValue;
        }

       
        #endregion
    }

}

