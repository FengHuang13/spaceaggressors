﻿using Anglia.CGTech.CKit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Anglia.CGTech.CKit.Helper;
using Anglia.CGTech.CKit.Helper;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Boolean Logic")]
    public class BooleanLogic : BooleanSource
    {

        [Header("Inputs")]
        [SerializeField]
        private BooleanLogicMode m_logicMode = BooleanLogicMode.And;
        [SerializeField]
        private BooleanSource[] m_sources;
        [Header ("Output")]
        [SerializeField]
        private bool m_currentValue;
        public override bool Fetch(bool force=false)
        {
            if (!m_calculatedThisFrame)
            {
                
                m_calculatedThisFrame = true;
                if (m_sources.Length>0)
                {
                    bool result=false;
                    bool firstValueSet = false;
                    for (int i = 0; i<m_sources.Length; i++)
                    {
                        BooleanSource currentSource = m_sources[i];
                        if (currentSource != null)
                        {
                            bool currentValue = currentSource.Fetch();
                            if (firstValueSet)
                            {
                                switch(m_logicMode)
                                {
                                    case BooleanLogicMode.And:
                                        result = result & currentValue;
                                        break;
                                    case BooleanLogicMode.Or:
                                        result = result | currentValue;
                                        break;
                                    case BooleanLogicMode.Xor:
                                        result = result ^ currentValue;
                                        break;
                                }
                            }
                            else
                            {
                                firstValueSet = true;
                                if (m_logicMode == BooleanLogicMode.Not)
                                {
                                    result = !currentValue;
                                }
                                else
                                {
                                    result = currentValue;
                                }
                            }
                        }
                    }
                    m_currentValue = result;
                }
                
            }
            return m_currentValue;
        }

        internal override bool FindInSources(DataSource dataObject)
        {
            return CheckForSource(dataObject, m_sources);
        }

        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }
        protected override void DrawGizmos()
        {
            
            for (int i = 0; i < m_sources.Length; i++)
            {
                if (m_sources[i] != null)
                {
                    GizmoHelper.DrawArrow(m_sources[i].transform.position, transform.position, GizmoHelper.KitType.Boolean);
                }
            }

        }
    }
}
