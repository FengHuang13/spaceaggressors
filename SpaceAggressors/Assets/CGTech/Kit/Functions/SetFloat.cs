﻿using Anglia.CGTech.CKit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Anglia.CGTech.CKit.Helper;
using Anglia.CGTech.CKit.Helper;

namespace Anglia.CGTech.CKit.Functions
{
    [AddComponentMenu("Construction Kit/Set Float")]
    public class SetFloat : FloatingPointSource
    {
        [Header("Inputs")]
        [SerializeField]
        private FloatingPointSource m_valueIfTrue;
        [SerializeField]
        private FloatingPointSource m_Default;
        [SerializeField]
        private BooleanSource m_set;
        [Header("Output")]
        [SerializeField]
        private float m_currentValue;

        public override float Fetch(bool force=false)
        {
            m_currentValue = m_Default.Fetch();
            if (m_valueIfTrue != null && m_Default != null)
            {
                m_currentValue = m_Default.Fetch();
                if (m_set == null || m_set.Fetch())
                {
                    m_currentValue = m_valueIfTrue.Fetch();
                }
            }
            return m_currentValue;
        }
        protected override GizmoHelper.PartType PartType
        {
            get
            {
                return GizmoHelper.PartType.Process;
            }
        }
        protected override void DrawGizmos()
        {
            if (m_valueIfTrue != null)
            {
                GizmoHelper.DrawArrow(m_valueIfTrue.transform.position, transform.position, GizmoHelper.KitType.Float);
            }

            if (m_Default != null)
            {
                GizmoHelper.DrawArrow(m_Default.transform.position, transform.position, GizmoHelper.KitType.Float);
            }

            if (m_set != null)
            {
                GizmoHelper.DrawArrow(m_set.transform.position, transform.position, GizmoHelper.KitType.Boolean);
            }
        }
    }
}
